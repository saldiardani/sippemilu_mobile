<?php
	#error_reporting ( 0 );
	header ( 'Content-Type: application/json' );
	require_once ( '../../libs/database.class.php' );
	require_once ( '../../libs/utility.class.php' );
	require_once ( '../../libs/authentication.class.php' );
	require_once ( '../../libs/activityapps.class.php' );
	require_once ( '../../libs/algoencrypt.class.php' );
	
	
	$db   = new Database();
	$algo = new Algo();
	$util = new Utility( $algo );
	$auth = new Authentication( $db, $util);
	$act  = new ActivityApps( $db, $util, $auth );
	
	if ( isset( $_POST ['type'] ) && !empty( $_POST['type'] ) )
	{
		switch ( $_POST['type'] )
		{
			case "reqlogincheck" :
				$auth->reqlogincheck($_POST['username'], $_POST['password']);
				break;

			case "reqaccountinfo" :
				$auth->reqaccountinfo($_POST['username']);
				break;
			
			case "reqaddaccount":
				$auth->reqaddaccount($_POST['username'], $_POST['level']);
				break;
			
			case "reqdelaccount" :
				$auth->reqdelaccount($_POST['id_user']);
				break;
			
			case "reqcoordinatordata" :
				$act->reqcoordinatordata($_POST['name'], $_POST['address'], $_POST['phone_number'], $_POST['email'], $_POST['id_province'], $_POST['id_city'], $_POST['id_restrict'], $_POST['pj']);
				break;
			
			case "reqsetelection" :
				$act->reqsetelection($_POST['election_name'],$_POST['id_prov'], $_POST['id_kota'], $_POST['id_kec'], $_POST['id_kel'], $_POST['date_exp']);
				break;
			
			case "reqdelelection":
				$act->reqdelelection($_POST['id_election_event']);
				break;

			case "reqgetcity":
				$act->reqgetcity($_POST['id_province']);
				break;
			
			case "reqgetkec":
				$act->reqgetkec($_POST['id_kokab']);
				break;
			
			case "reqgetkel":
				$act->reqgetkel($_POST['id_kec']);
				break;
			
			case "reqgettps":
				$act->reqgettps($_POST['id_kel']);
				break;
			case "reqinsertcandidate":
				$act->reqinsertcandidate($_POST['candidate_name'], $_POST['id_election_event'], $_POST['id_pemenangan']);
				break;
			
			case "reqdeletecandidate":
				$act->reqdeletecandidate($_POST['id_candidate']);
				break;

			case "requpdatecandidate" :
				$act->requpdatecandidate($_POST['id_candidate'], $_POST['candidate_name'], $_POST['id_election_event'], $_POST['id_pemenangan']);
				break;
			
			case "reqmarkdptdata":
				$act->reqmarkdptdata($_POST['id_dpt'],$_POST['id_mark'], $_POST['id_election'],$_POST['id_candidate']);
				break;
			
			case "reqgetdptdata":
				$act->reqgetdptdata($_POST['nik']);
				break;

			case "reqaddphonedpt" :
				$act->reqaddphonedpt($_POST['phone'],$_POST['nik']);
				break;

			default :
				unknowRequest();
				break;
		}
	}
	else
	{
		unknowRequest();
	}
	
	function unknowRequest()
	{
		$dataRespons = [];
		
		array_push($dataRespons,
		[
			'type' => 'unknown request',
			'reponse' => 'sorry...server could not understand the request'
		]);
		
		echo json_encode($dataRespons, JSON_PRETTY_PRINT);
	}
?>