<?php

//header ( 'Content-Type: application/json' );
require_once ( '../../libs/database.class.php' );
require_once ( '../../libs/utility.class.php' );
require_once ( '../../libs/authentication.class.php' );
require_once ( '../../libs/activityapps.class.php' );
require_once ( '../../libs/algoencrypt.class.php' );
require_once("../../components/excel_reader2.php");

$db   = new Database();
$algo = new Algo();
$util = new Utility( $algo );
$auth = new Authentication( $db, $util);
$act  = new ActivityApps( $db, $util, $auth );

$data   = new Spreadsheet_Excel_Reader($_FILES['userfile']['tmp_name']);
$baris  = $data->rowcount($sheet_index=0);

for ($i=2; $i <= $baris; $i++)
{
    $nokk          = $data->val($i, 1);
    $nik           = $data->val($i, 2);
    $nama_pemilih  = $data->val($i, 3);
    $tempat_lahir  = $data->val($i, 4);
    $tanggal_lahir = $data->val($i, 5);
    $umur          = $data->val($i, 6);
    $status_kawin  = $data->val($i, 7);
    $jk            = $data->val($i, 8);
    $jalan         = $data->val($i, 9);
    $rt            = $data->val($i, 10);

    $rw            = $data->val($i, 11);
    $ket           = $data->val($i, 12);
    $id_provinsi   = $data->val($i, 13);
    $id_kota       = $data->val($i, 14);
    $id_kec        = $data->val($i, 15);
    $id_kel        = $data->val($i, 16);
    $tps           = $data->val($i, 17);
    $id_election_event = $data->val($i, 18);
    $id_mark       = $data->val($i, 19);
    $id_candidate  = $data->val($i, 20);

    $query = "INSERT INTO tbl_dpt(nokk,nik,nama_pemilih,tempat_lahir,tanggal_lahir,umur,status_kawin,jk,jalan,rt,rw,ket,id_province,id_kota,id_kec,id_kel,tps,id_election_event,id_mark,id_candidate) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    $insrt = $db->insertValue($query,[$nokk,$nik,$nama_pemilih,$tempat_lahir,$tanggal_lahir,$umur,$status_kawin,$jk,$jalan,$rt,$rw,$ket,$id_provinsi,$id_kota,$id_kec,$id_kel,$tps,$id_election_event,$id_mark,$id_candidate]);
}
?>

<script type="text/javascript">
    alert('berhasil di upload');
    document.location.href="../../dptInsert";
</script>