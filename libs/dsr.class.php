<?php

class DSR
{

    public $dataResponse;
    public $responseType;
    public $responseState;
    public $responseData;
    public $responseIdData;

    public function __construct()
    {

    }

    public function clearDataResponse()
    {
        $this->dataResponse = [];
    }

    public function clearResponseType()
    {
        $this->responseType = null;
    }

    public function clearResponseState()
    {
        $this->responseState = null;
    }

    public function clearResponseData()
    {
        $this->responseData = null;
    }

    public function clearResponseIdData()
    {
        $this->responseIdData = null;
    }


    public function clearDataTypeState()
    {
        self::clearDataResponse();
        self::clearResponseType();
        self::clearResponseState();
        self::clearResponseData();
    }

    public function setResponseType($response)
    {
        $this->responseType = $response;
    }

    public function setResponseState($state)
    {
        $this->responseState = $state;
    }

    public function setResponseData($data)
    {
        $this->responseData = $data;
    }

    public function setResponseIdData($data)
    {
        $this->responseIdData = $data;
    }

    public function getResponseType()
    {
        return $this->responseType;
    }

    public function getResponseState()
    {
        return $this->responseState;
    }

    public function getResponseData()
    {
        return $this->responseData;
    }

    public function getResponseIdData()
    {
        return $this->responseIdData;
    }
}


?>