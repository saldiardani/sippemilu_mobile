<?php

require_once('dsr.class.php');

class Authentication extends DSR
{

    private $db;
    private $util;

    public function __construct( $db, $util )
    {
        $this->db   = $db;
        $this->util = $util;
    }

    public function reqlogincheck($username,$password)
    {
        parent::clearDataTypeState();
        parent::setResponseType('reslogincheck');
        parent::setResponseState(false);

        $username   = $this->util->sanitation($username);
        $password   = $this->util->encrypt_decrypt('encrypt', $password);
        
        $query      = "SELECT * FROM tbl_user WHERE tbl_user.username=? AND tbl_user.password=?";
        $chkData    = $this->db->getValue($query,[$username,$password]);

        (!empty($chkData)) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState(),
            'id_level'  => $chkData['id_level']
        ]);

        echo json_encode($this->dataResponse,JSON_PRETTY_PRINT); 
    }

    public function reqaddaccountuser($username,$level)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resaddaccountuser');
        parent::setResponseState(false);

        $username   = $this->util->sanitation($username);
        $level      = $this->util->sanitation($level);
        $randomStr  = "";
     
        if(self::chkusernameexisted())
        {
            $randomStr  = $this->util->randomString(4);
            $password   = $this->util->encrypt_decrypt('encrypt',$randomStr);

            $query  = "INSERT INTO tbl_user(username,password,level) VALUES(?,?,?)";
            $insrt  = $this->db->insertValue($query,[$username,$password,$level]);

            ($insrt) ?  parent::setResponseState(true) : parent::setResponseState(false);
        }
        
        array_push($this->dataResponse,
        [
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState(),
            'password'  => $randomStr
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }

    public function reqdelaccountuser($id_user)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resdelaccountuser');
        parent::setResponseState(false);

        $id_user = $this->util->sanitation($id_user);

        $query  = "DELETE FROM tbl_user WHERE tbl_user.id_user=?";
        $del    = $this->db->deleteValue($query,[$id_user]);

        ($del) ? parent::setResponseState(true) : parent::setResponseState(false);

        array($this->dataResponse,
        [
            'type'  => parent::getResponseType(),
            'state' => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse,JSON_PRETTY_PRINT);
    }

    public function chkusernameexisted($username)
    {
        $result = false;

        $query = "SELECT * FROM tbl_user WHERE tbl_user.username=?";
        $chkusername = $this->db->getValue($query,[$username]);
        
        (empty($chkusername)) ? $result = true : $result = false;

        return $result;
    }
   
}

?>
