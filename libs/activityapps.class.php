<?php

require_once('dsr.class.php');

class ActivityApps extends DSR
{
    private $db;
    private $util;
    private $auth;

    public function __construct( $db, $util, $auth )
    {
        $this->db   = $db;
        $this->util = $util;
        $this->auth = $auth;
    }


    public function reqstorevote($data, $id_dpt)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resstorevote');
        parent::setResponseState(false);

        $id_dpt     = $this->util->sanitation($id_dpt);
        $data_array = $this->util->setStringToArray( "|", $data );

        $id_election_event = self::geteventfromidpt($id_dpt); 

        if(self::chkdpthassend($id_dpt))
        {
            foreach($data_array as $data)
            {
                $data_vote  = $this->util->setStringToArray(":", $data);
                $query      = "UPDATE tbl_vote_data SET vote=? WHERE tbl_vote_data.id_dpt=? AND tbl_vote_data.id_candidate=?";
                $updt       = $this->db->updateValue($query,[$data_vote[1],$id_dpt,$data_vote[0]]);

                ($updt) ?  parent::setResponseState(true) :  parent::setResponseState(false);
            }
        }
        else
        {
            foreach($data_array as $data)
            {
                $data_vote  = $this->util->setStringToArray(":", $data);
                $query      = "INSERT INTO tbl_vote_data(id_candidate,id_dpt,vote,id_event) VALUES(?,?,?,?)";
                $insrt      = $this->db->insertValue($query,[$data_vote[0],$id_dpt,$data_vote[1],$id_election_event]);

                ($insrt) ? parent::setResponseState(true) : parent::setResponseState(false);
            }
        }

        array_push($this->dataResponse,
        [
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse,JSON_PRETTY_PRINT);
    }


    public function reqinsertcandidate($candidate_name, $id_election_event, $id_pemenangan)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resinsertcandidate');
        parent::setResponseState(false);

        $candidate_name     = $this->util->sanitation($candidate_name);
        $id_election_event  = $this->util->sanitation($id_election_event);
        $id_pemenangan      = $this->util->sanitation($id_pemenangan);

        $query  = "INSERT INTO tbl_candidate(candidate_name, id_election_event, id_pemenangan) VALUES(?,?,?)";
        $insrt  = $this->db->insertValue($query,[$candidate_name, $id_election_event, $id_pemenangan]);

        ($insrt) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [   
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse,JSON_PRETTY_PRINT);
    }


    public function requpdatecandidate($id_candidate,$candidate_name,$id_election_event,$id_pemenangan)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resupdatecandidate');
        parent::setResponseState(false);

        $id_candidate       = $this->util->sanitation($id_candidate);
        $candidate_name     = $this->util->sanitation($candidate_name);
        $id_election_event  = $this->util->sanitation($id_election_event);
        $id_pemenangan      = $this->util->sanitation($id_pemenangan);

        $query = "UPDATE tbl_candidate SET candidate_name=?, id_election_event=?, id_pemenangan=? WHERE tbl_candidate.id_candidate=?";
        $insrt = $this->db->insertValue($query,[$candidate_name, $id_election_event, $id_pemenangan, $id_candidate ]);

        $insrt ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type' => parent::getResponseType(),
            'state'=> parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }


    public function reqdeletecandidate($id_candidate)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resdeletecandidate');
        parent::setResponseState(false);

        $id_candidate = $this->util->sanitation($id_candidate);

        $query  = "DELETE FROM tbl_candidate WHERE tbl_candidate.id_candidate=?";
        $del    = $this->db->deleteValue($query,[$id_candidate]);

        ($del) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'  => parent::getResponseType(),
            'state' => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }

    public function getNameFromDPPTData($id_dpt)
    {
        $query      = "SELECT * FROM tbl_dpt WHERE tbl_dpt.id_dpt=?";
        $getData    = $this->db->getValue($query,[$id_dpt]);

        return $getData['nama_pemilih'].substr($getData['nik'],0,3);
    }

    public function reqmarkdptdata($id_dpt,$id_mark,$id_election,$id_candidate)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resmarkdptdata');
        parent::setResponseState(false);
        
        $default_pass   = $this->util->encrypt_decrypt('encrypt', (int)1234);

        $id_dpt         = $this->util->sanitation($id_dpt);
        $id_mark        = $this->util->sanitation($id_mark);
        $id_election    = $this->util->sanitation($id_election);
        $id_candidate   = $this->util->sanitation($id_candidate);

        $username   = self::getNameFromDPPTData($id_dpt);

        if($id_election>0)
        {
            $usr_chk    = $this->auth->chkusernameexisted($username);
            $id_level   = (int) 3;

            if($usr_chk)
            {
                $query  = "INSERT INTO tbl_user(username,password,id_level,id_dpt) VALUES(?,?,?,?)";
                $insrt  = $this->db->insertValue($query,[$username,$default_pass,$id_level,$id_dpt]);
                ($insrt) ? parent::setResponseState(true) : parent::setResponseState(false);
            }
            else
            {
                $query  = "UPDATE tbl_user SET tbl_user.id_level=? WHERE tbl_user.id_dpt=?";
                $updt   = $this->db->updateValue($query,[$id_level,$id_dpt]);
                ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);
            }
            
            $query  = "UPDATE tbl_dpt SET tbl_dpt.id_mark=?, tbl_dpt.id_election_event=?, tbl_dpt.id_candidate=? WHERE tbl_dpt.id_dpt=?"; 
            $updt   = $this->db->updateValue($query,[$id_mark,$id_election,$id_candidate,$id_dpt]);
            
            ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);
        }
        else if($id_election==0 AND $id_candidate==0)
        { 
            echo $usr_chk    = $this->auth->chkusernameexisted($username);
            $id_level   = (int) 2;
            
            if($usr_chk)
            {
                $query  = "INSERT INTO tbl_user(username,password,id_level,id_dpt) VALUES(?,?,?,?)";
                $insrt  = $this->db->insertValue($query,[$username,$default_pass,$id_level,$id_dpt]);
                ($insrt) ? parent::setResponseState(true) : parent::setResponseState(false);
            }
            else
            {
                $query  = "UPDATE tbl_user SET tbl_user.id_level=? WHERE tbl_user.id_dpt=?";
                $updt   = $this->db->updateValue($query,[$id_level,$id_dpt]);
                ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);
            }
           
            $query  = "UPDATE tbl_dpt SET tbl_dpt.id_mark=?, tbl_dpt.id_election_event=?, tbl_dpt.id_candidate=? WHERE tbl_dpt.id_dpt=?"; 
            $updt   = $this->db->updateValue($query,[$id_mark,$id_election,$id_candidate,$id_dpt]);

            ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);
        }
        else if($id_candidate>0)
        { 
            $usr_chk    = $this->auth->chkusernameexisted($username);

            if(!$usr_chk)
            {
                $query  = "DELETE FROM tbl_user WHERE tbl_user.id_dpt=?";
                $del    = $this->db->deleteValue($query,[$id_dpt]);
            }

            $query  = "UPDATE tbl_dpt SET tbl_dpt.id_mark=?, tbl_dpt.id_election_event=?, tbl_dpt.id_candidate=? WHERE tbl_dpt.id_dpt=?"; 
            $updt   = $this->db->updateValue($query,[$id_mark,$id_election,$id_candidate,$id_dpt]);
    
            ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);
        }
        else
        {

        }

        array_push($this->dataResponse,
        [
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse,JSON_PRETTY_PRINT);
    }

    

    public function reqsetelection( $election_name, $id_prov, $id_kota, $id_kec, $id_kel,$date_exp )
    {
        parent::clearDataTypeState();
        parent::setResponseType('ressetelection');
        parent::setResponseState(false);

        $election_name  = $this->util->sanitation($election_name);
        $id_prov        = $this->util->sanitation($id_prov);
        $id_kota        = $this->util->sanitation($id_kota);
        $id_kec         = $this->util->sanitation($id_kec);
        $id_kel         = $this->util->sanitation($id_kel);
        $date_exp       = $this->util->sanitation($date_exp);
        $date_reg       = $this->util->setRegisterDate( $date_exp );
        $date_exp       = $this->util->changeFormatDateFromDateTimetoDate( $date_exp );

        $query = "INSERT INTO tbl_election_event(election_event_name, id_provinsi, id_kabupaten, id_kecamatan, id_kelurahan, date_exp, date_reg) VALUES(?,?,?,?,?,?,?)";
        $insrt = $this->db->insertValue($query,[$election_name,$id_prov,$id_kota,$id_kec,$id_kel,$date_exp,$date_reg]);

        ($insrt) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse,JSON_PRETTY_PRINT);
    }


    public function requpdatelection( $id_election_event, $election_event_name)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resupdatelection');
        parent::setResponseState(false);

        $id_election_event      = $this->util->sanitation($id_election_event);
        $election_event_name    = $this->util->sanitation($election_event_name); 

        $query  = "UPDATE tbl_election_event SET tbl_election_event.election_event_name=? WHERE tbl_election_event.id_election_event=?";
        $updt   = $this->db->updateValue($query,[$election_event_name, $id_election_event]);
        
        ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse,JSON_PRETTY_PRINT);
    }


    public function reqdelelection( $id_election_event )
    {
        parent::clearDataTypeState();
        parent::setResponseType('resdelelection');
        parent::setResponseState(false);

        $id_election_event = $this->util->sanitation($id_election_event);

        $query  = "DELETE FROM tbl_election_event WHERE tbl_election_event.id_election_event=?";
        $del    = $this->db->deleteValue($query,[$id_election_event]);

        ($del) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'      => parent::getResponseType(),
            'state'     => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }


    public function chkdpthassend($id_dpt)
    {
        $result = false;

        $query = "SELECT * FROM tbl_vote_data WHERE tbl_vote_data.id_vote_data=?";
        $chkresult = $this->db->getValue($query,[$id_dpt]);

        (!empty($chkresult)) ? $result = true  : $result = false;

        return $result;
    }

    public function reqaddphonedpt($phone,$nik)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resaddphonedpt');
        parent::setResponseState(false);

        $phone  = $this->util->sanitation($phone);
        $nik    = $this->util->sanitation($nik);

        $query  = "UPDATE tbl_dpt SET telepon=? WHERE tbl_dpt.nik=?";
        $updt   = $this->db->updateValue($query,[$phone,$nik]);

        ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);
        
        array_push($this->dataResponse,
        [
            'type'  => parent::getResponseType(),
            'state' => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }

    public function reqsetsurveyquest($question, $id_election_event)
    {
        parent::clearDataTypeState();
        parent::setResponseType('ressetsurveyquest');
        parent::setResponseState(false);

        $question           = $this->util->sanitation($question);
        $id_election_event  = $this->util->sanitation($id_election_event);

        $query  = "INSERT INTO tbl_reqsetsurveyquest(question, id_election_event) VALUES(?,?)";
        $insrt  = $this->db->inserValue($query,[$question, $id_election_event]);

        ($insrt) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'  => parent::getResponseType(),
            'state' => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }


    public function reqdelsurveyrequest($id_survey)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resdelsurveyrequest');
        parent::setResponseState(false);

        $query  = "DELETE FROM tbl_survey WHERE tbl_survey.id_survey=?";
        $del    = $this->db->insertValue($query,[$id_survey]);

        ($del) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'  => parent::getResponseType(),
            'state' => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }


    public function requpdatesurveyquest($id_survey)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resupdatesurveyquest');
        parent::setResponseState(false);

        $id_survey  = $this->util->sanitation($id_survey);
        
        $query  = "UPDATE FROM tbl_survey SET WHERE tbl_survey.id_survey=?";
        $updt   = $this->db->updateValue($query,[$id_survey]);

        ($updt) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'  => parent::getResponseType(),
            'state' => parent::getResponseState()
        ]);

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }


    public function geteventfromidpt($id_dpt)
    {
        $query       = "SELECT * FROM tbl_user INNER JOIN tbl_dpt ON tbl_user.id_dpt = tbl_dpt.id_dpt WHERE tbl_user.id_dpt=?";
        $getEvent   = $this->db->getValue($query,[$id_dpt]);

        return $getEvent['id_election_event'];
    }


    public function reqgetcity($id_province)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resgetcity');
        parent::setResponseState(false);

        $id_province = $this->util->sanitation($id_province);

        $query   = "SELECT * FROM tbl_kabupaten WHERE id_prov=? ORDER BY tbl_kabupaten.nama_kab ASC";
        $getData = $this->db->getAllValue($query,[$id_province]);

        if(!empty($getData))
        {
            parent::setResponseState(true);

            foreach($getData as $data)
            {
                parent::setResponseData($data['nama_kab']);
                parent::setResponseIdData($data['id_kab']);

                array_push($this->dataResponse,
                [
                    'type'  => parent::getResponseType(),
                    'state' => parent::getResponseState(),
                    'name'  => parent::getResponseData(),
                    'id_kab'=> parent::getResponseIdData()
                ]);

            }
        }
        else
        {
            array_push($this->dataResponse,
            [
                'type'  => parent::getResponseType(),
                'state' => parent::getResponseState()
            ]);
        }
        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }

    public function reqgettps($id_kel)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resgettps');
        parent::setResponseState(false);

        $id_kel = $this->util->sanitation($id_kel);

        $query      = "SELECT DISTINCT tps FROM tbl_dpt WHERE tbl_dpt.id_kel=?";
        $getData    = $this->db->getAllValue($query,[$id_kel]);

        if(!empty($getData))
        {
            parent::setResponseState(true);

            foreach($getData as $data)
            {
                parent::setResponseData($data['tps']);

                array_push($this->dataResponse,
                [
                    'type'  => parent::getResponseType(),
                    'state' => parent::getResponseState(),
                    'tps'   => parent::getResponseData(),
                ]);

            }
        }
        else
        {
            array_push($this->dataResponse,
            [
                'type'  => parent::getResponseType(),
                'state' => parent::getResponseState()
            ]);
        }
        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }

    public function reqgetkec($id_kokab)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resgetkec');
        parent::setResponseState(false);

        $id_kokab = $this->util->sanitation($id_kokab);

        $query   = "SELECT * FROM tbl_kecamatan WHERE id_kab=? ORDER BY tbl_kecamatan.nama_kec ASC";
        $getData = $this->db->getAllValue($query,[$id_kokab]);

        if(!empty($getData))
        {
            parent::setResponseState(true);

            foreach($getData as $data)
            {
                parent::setResponseData($data['nama_kec']);
                parent::setResponseIdData($data['id_kec']);

                array_push($this->dataResponse,
                [
                    'type'  => parent::getResponseType(),
                    'state' => parent::getResponseState(),
                    'name'  => parent::getResponseData(),
                    'id_kec'=> parent::getResponseIdData()
                ]);

            }
        }
        else
        {
            array_push($this->dataResponse,
            [
                'type'  => parent::getResponseType(),
                'state' => parent::getResponseState()
            ]);
        }
        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }


    public function reqgetkel($id_kec)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resgetkel');
        parent::setResponseState(false);

        $id_kec = $this->util->sanitation($id_kec);

        $query   = "SELECT * FROM tbl_kelurahan WHERE id_kec=? ORDER BY tbl_kelurahan.nama_kel ASC";
        $getData = $this->db->getAllValue($query,[$id_kec]);

        if(!empty($getData))
        {
            parent::setResponseState(true);

            foreach($getData as $data)
            {
                parent::setResponseData($data['nama_kel']);
                parent::setResponseIdData($data['id_kel']);

                array_push($this->dataResponse,
                [
                    'type'  => parent::getResponseType(),
                    'state' => parent::getResponseState(),
                    'name'  => parent::getResponseData(),
                    'id_kel'=> parent::getResponseIdData()
                ]);

            }
        }
        else
        {
            array_push($this->dataResponse,
            [
                'type'  => parent::getResponseType(),
                'state' => parent::getResponseState()
            ]);
        }
        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }

    public function reqgetdptdata($nik)
    {
        parent::clearDataTypeState();
        parent::setResponseType('resgetdptdata');
        parent::setResponseState(false);

        $nik = $this->util->sanitation($nik);

        $query      = "SELECT * FROM tbl_dpt WHERE tbl_dpt.nik=?";
        $getData    = $this->db->getValue($query,[$nik]);

        ($getData) ? parent::setResponseState(true) : parent::setResponseState(false);

        array_push($this->dataResponse,
        [
            'type'          => parent::getResponseType(),
            'state'         => parent::getResponseState(),
            'nokk'          => $getData['nokk'],
            'nik'           => $getData['nik'],
            'nama_pemilih'  => $getData['nama_pemilih'],
            'tempat_lahir'  => $getData['tempat_lahir'],
            'tanggal_lahir' => $getData['tanggal_lahir'],
            'umur'          => $getData['umur'],
            'status_kawin'  => $getData['status_kawin'],
            'jk'            => $getData['jk'],
            'jalan'         => $getData['jalan'],
            'rt'            => $getData['rt'],
            'rw'            => $getData['rw'],
            'ket'           => $getData['ket']
        ]); 

        echo json_encode($this->dataResponse, JSON_PRETTY_PRINT);
    }

}

?>