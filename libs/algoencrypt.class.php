<?php

class Algo
{

    private $algoEncrypt;
    private $securePass;
    /*
        [0] => AES-128-CBC
    [1] => AES-128-CFB
    [2] => AES-128-CFB1
    [3] => AES-128-CFB8
    [5] => AES-128-OFB
    [6] => AES-192-CBC
    [7] => AES-192-CFB
    [8] => AES-192-CFB1
    [9] => AES-192-CFB8
    [11] => AES-192-OFB
    [12] => AES-256-CBC
    [13] => AES-256-CFB
    [14] => AES-256-CFB1
    [15] => AES-256-CFB8
    [17] => AES-256-OFB
    [18] => BF-CBC
    [19] => BF-CFB
    [21] => BF-OFB
    [22] => CAST5-CBC
    [23] => CAST5-CFB
    [25] => CAST5-OFB
    [41] => IDEA-CBC
    [42] => IDEA-CFB
    [44] => IDEA-OFB
    [53] => aes-128-cbc
    [54] => aes-128-cfb
    [55] => aes-128-cfb1
    [56] => aes-128-cfb8
    [58] => aes-128-ofb
    [59] => aes-192-cbc
    [60] => aes-192-cfb
    [61] => aes-192-cfb1
    [62] => aes-192-cfb8
    [64] => aes-192-ofb
    [65] => aes-256-cbc
    [66] => aes-256-cfb
    [67] => aes-256-cfb1
    [68] => aes-256-cfb8
    [70] => aes-256-ofb
    [71] => bf-cbc
    [72] => bf-cfb
    [74] => bf-ofb
    [75] => cast5-cbc
    [76] => cast5-cfb
    [78] => cast5-ofb
    [94] => idea-cbc
    [95] => idea-cfb
    [97] => idea-ofb
     */

    public function __construct()
    {
        $this->securePass = 'pilkada 2018';
    }
    
    public function setSecurePass($securePass)
    {
        $this->securePass = $securePass;
    }

    public function getSecurePass()
    {
        return $this->securePass;
    }

    public function AES128CBC()
    {
        return $this->algoEncrypt = 'AES-128-CBC';
    }

    public function AES128CFB()
    {
        return $this->algoEncrypt = 'AES-128-CFB';
    }

    public function AES128CFB8 ()
    {
        return $this->algoEncrypt = 'AES-128-CFB8';
    }

    public function AES192CBC()
    {
        return $this->algoEncrypt = 'AES-192-CBC';
    }
}


?>
