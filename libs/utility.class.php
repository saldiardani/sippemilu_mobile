<?php

class Utility
{
    protected $temp_variable = null;
    protected $skey          = "realfeelrealfeel";
    
    private $emailSenderAcc;
    private $emailSenderPass;
    private $algo;
    
    public function __construct( $algo )
    {
        $this->algo = $algo;
    }
    
    public function startSession()
    {
        ob_start();
        session_start();
    }

    public function hideErrorReporting()
    {
        error_reporting( 0 );
    }

    public function code_activation()
    {
        return md5( uniqid( rand() ) );
    }

    public function getTrxCode()
    {
        return strtoupper( uniqid() );
    }

    public function registration_id()
    {
        $this->setDefaultTimeZone( "Asia/Bangkok" );

        return date( "Ymd" ) . time();
    }

    public function setDefaultTimeZone( $timezones )
    {
        date_default_timezone_set( $timezones );
    }

    public function getDateTimeToday()
    {
        $respons = date( "Y-m-d H:i:s" );
        return $respons;
    }

    public function getCurrentTime()
    {
        $respons = date( "H:i:s" );
        return $respons;
    }

    public function getDateToday()
    {
        $respons = date( "d-m-Y" );
        return $respons;
    }

    public function setRegisterDate( $date_data )
    {
        $respons = date( "Ymd",strtotime( $date_data ) );
        return $respons;
    }

    public function changeFormatDateFromNumberToString( $date_number )
    {
        $respons = date( "d F Y",strtotime( $date_number ) );
        return $respons;
    }

    public function changeFormatDateFromDateTimetoDate( $date_time )
    {
        return date( "Y-m-d",strtotime( $date_time ) );
    }

    public function getDateBeforeToday( $date,
                                        $number )
    {
        $this->setDefaultTimeZone( 'Asia/Bangkok' );

        return date( 'Y-m-d',strtotime( $date . '-' . $number . ' day' ) );
    }

    public function getDateAfterToday( $date,
                                       $number )
    {
        $this->setDefaultTimeZone( 'Asia/Bangkok' );

        return date( 'Y-m-d',
                     strtotime( $date . '+' . $number . ' day' ) );
    }

    public function getDayState( $time )
    {
        $hour  = substr($time,0,2);
        $state = null;

        if ( $hour < 12 )
        {
            $state = "Pagi";
        }
        else if ( $hour < 17 AND $hour >= 12 )
        {
            $state = "Sore";
        }
        else
        {
            $state = "Malam";
        }

        return $state;
    }

    public function getDayInIndonesia( $day )
    {
        if ( $day === "Monday" )
        {
            return $day = "Senin";
        }
        else if ( $day === "Tuesday" )
        {
            return $day = "Selasa";
        }
        else if ( $day === "Wednesday" )
        {
            return $day = "Rabu";
        }
        else if ( $day === "Thrusday" )
        {
            return $day = "Kamis";
        }
        else if ( $day === "Friday" )
        {
            return $day = "Jum'at";
        }
        else if ( $day === "Saturday" )
        {
            return $day = "Sabtu";
        }
        else if ( $day === "Sunday" )
        {
            return $day = "Minggu";
        }
        else
        {
            
        }
    }

    public function getEndTime( $start_time,$duration )
    {
        $end_time = ((substr($start_time,0,2)) + $duration );

        if ( $end_time < 10 )
        {
            $end_time = "0" . $end_time . ":00";
        }
        else
        {
            $end_time = $end_time . ":00";
        }

        return $end_time;
    }

    public function highestValue( array $numbers,
                                  $highest )
    {
        $maxHeap = new SplMaxHeap;

        foreach ( $numbers as $number )
        {
            $maxHeap->insert( $number );
        }

        return iterator_to_array( new LimitIterator( $maxHeap, 0, $highest));
    }

    public function dateRangeComparation( $date_start, $date_end, $date_comparation)
    {
        $respons = false;
        if ($date_comparation <= $date_end && $date_comparation >= $date_start)
        {
            $respons = true;
        }
        else
        {
            $respons = false;
        }

        return $respons;
    }

    public function limitDate( $date_today,$date_end_promotion )
    {
        $respons = null;

        if ( $date_today <= $date_end_promotion )
        {
            $respons = 1;
        }
        else
        {
            $respons = 0;
        }

        return $respons;
    }

    public function encryptpass( $word )
    {
        $respons=md5( $word );
        return $respons;
    }


    public function encode( $word )
    {
        if(!$word)
        {
            return false;
        }

        $text      = $word;
        $iv_size   = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB);
        $iv        = mcrypt_create_iv( $iv_size,MCRYPT_RAND );
        $crypttext = mcrypt_encrypt( MCRYPT_RIJNDAEL_256,$this->skey, $text, MCRYPT_MODE_ECB, $iv);

        return trim( $this->safe_b64encode( $crypttext ) );
    }

    public function safe_b64encode( $string )
    {
        $data = base64_encode( $string );
        $data = str_replace(
                [ '+', '/', '=' ],
                [ '-', '_', '' ],
                $data
        );

        return $data;
    }

    public function decode( $word )
    {
        if(!$word)
        {
            return false;
        }

        $crypttext   = $this->safe_b64decode($word);
        $iv_size     = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256,MCRYPT_MODE_ECB);
        $iv          = mcrypt_create_iv( $iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$this->skey, $crypttext, MCRYPT_MODE_ECB,$iv);

        return trim( $decrypttext );
    }

    public function safe_b64decode( $string )
    {
        $data = str_replace( [ '-', '_' ],
                             [ '+', '/' ],
                             $string );
        $mod4 = strlen( $data ) % 4;

        if ( $mod4 )
        {
            $data .= substr( '====',$mod4 );
        }

        return base64_decode( $data );
    }

    public function encrypt_decrypt($action, $string) 
    {
        $output = false;
    
        $encrypt_method = $this->algo->AES192CBC();
        $secret_key     = $this->algo->getSecurePass();
        $secret_iv      = $this->algo->getSecurePass();
    
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
    
        if( $action == 'encrypt' ) 
        {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' )
        {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }


    public function uppercaseFirstCharaEachWord( $word )
    {
        return ucwords(strtolower($word));
    }

    public function randomCodeGenerator()
    {
        return mt_rand(10,100 ).$this->setDateRegisterForToday().time();
    }

    public function setDateRegisterForToday()
    {
        $respons = date( "Ymd" );

        return $respons;
    }

    public function compareTwoArrayReturnMatch($array1,$array2 )
    {
        return array_intersect($array1,$array2 );
    }

    public function compareTwoArrayReturnDifferent($array1,$array2 )
    {
        return array_diff($array1,$array2 );
    }

    public function setStringToArray( $delimeter, $word )
    {
        return explode( $delimeter, $word );
    }

    public function calcArrayIndex( $array_data )
    {
        return sizeof( $array_data );
    }

    public function sanitation( $word )
    {
        return htmlentities(addslashes( $word ) );
    }

    public function setEmailSender( $mail )
    {
        $this->emailSenderAcc = $mail;
    }

    public function setEmailSenderPass( $pass )
    {
        $this->emailSenderPass = $pass;
    }

    public function sendEmail( $mail_receiver, $message, $subject )
    {
        require_once ( 'class.phpmailer.php');

        $mail             = new PHPMailer;
        $mail->IsSMTP();
        $mail->SMTPSecure = 'ssl';
        $mail->Host       = 'myflorist.id'; //hostname masing-masing provider email
        $mail->SMTPDebug  = 0; //2 for debug
        $mail->Port       = 465;
        $mail->SMTPAuth   = true;
        $mail->Username   = 'system@myflorist.id'; //user email
        $mail->Password   = 'zaq1xsw2'; //password email
        $mail->SetFrom( 'system@myflorist.id',
                        'MyFlorist System' ); //set email pengirim
        $mail->Subject    = $subject; //subyek email
        $mail->AddAddress( $mail_receiver,
                           ' ' ); //tujuan email
        $mail->IsHTML( true );
        $mail->MsgHTML( $message );
        return ($mail->Send()) ? 'true' : 'false';
    }

    public function randomString( $length )
    {
        $str        = "";
        $characters = array_merge( range('A','Z'), range('a','z'));
        $max        = count( $characters )-1;
        
        for ( $i = 0; $i < $length; $i++ )
        {
            $rand = mt_rand(0,$max);
            $str  .= $characters[$rand];
        }
        return $str;
    }

}

?>
