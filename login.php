<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="../img/slim-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="../img/slim-social.html">
    <meta property="og:image:secure_url" content="../img/slim-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Slim Responsive Bootstrap 4 Admin Template</title>

    <!-- Vendor css -->
    <link href="lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="lib/Ionicons/css/ionicons.css" rel="stylesheet">

    <!-- Slim CSS -->
    <link rel="stylesheet" href="css/slim.css">

  </head>
  <body>

    <div class="signin-wrapper">

      <div class="signin-box">
        <h2 class="slim-logo"><a href="#">SIPPEMILU<span></span></a></h2>
        <h5 class="signin-title-primary" style="font-size:14px">Selamat datang kembali</h5>
        <h3 class="signin-title-secondary">Login untuk masuk</h3>

        <div class="form-group">
          <input type="text" class="form-control" id="username" placeholder="Nomor Telepon" required autocomplete="off">
        </div>
        <div class="form-group mg-b-50">
          <input type="password" class="form-control" id="password" placeholder="Kata sandi" required autocomplete="off">
        </div>
        <button class="btn btn-primary btn-block btn-signin" id="login">Masuk</button>
        <!-- <p class="mg-b-0">Don't have an account? <a href="page-signup.html">Sign Up</a></p> -->
      </div><!-- signin-box -->

    </div><!-- signin-wrapper -->

        <script src="lib/jquery/js/jquery.js"></script>
        <script src="lib/popper.js/js/popper.js"></script>
        <script src="lib/bootstrap/js/bootstrap.js"></script>
        <script src="js/slim.js"></script>
  </body>
</html>


<script type="text/javascript">

function loginProcess()
{
    console.log('try enter page');

    var username = $("#username").val();
    var password = $("#password").val();

    if( username=="" || password=="" )
    {
        alert('mohon field username dan password diisi');
    }
    else
    {
        $.ajax
        ({
            type        : 'POST',
            url         : 'API/web/sippemilu.php',
            data        : 'type=reqlogincheck'+'&username='+username+'&password='+password,
            dataType    : 'JSON',
            cache       : false,
            success     : function(response)
            {
                for(key in response)
                {
                    if(response.hasOwnProperty(key))
                    {
                        if(response[key]['state'])
                        {
                            console.info('ready for redirect...');
                            
                            var id_level = response[key]['id_level'];

                            setCookies("username", username, 365);
                            setCookies("id_level", id_level, 365);
                            document.location.href = "main";
                        }
                    }
                }
            }
        });
    }
}


function setCookies(cname, cvalue, exdays)
{
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function readyApps()
{
    $('#login').click(loginProcess)
}

$(document).ready(readyApps);
</script>
