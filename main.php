<!DOCTYPE html>
<html lang="en">
  <?php 
    header_remove('Access-Control-Allow-Origin');
    header('Access-Control-Allow-Origin: *');
    require_once('components/header.php');

    require_once('libs/database.class.php');


    $db = new Database();
  ?>



   <script src="lib/jquery/js/jquery.js"></script>
  <body>
    <?php
        require_once('components/header_internal.php');
    ?>

    <!-- place for slim navbar -->

    <div class="slim-mainpanel">
      <div class="container">
        <div class="row row-xs" style="margin-top: 10px;">
            <?php
                
                $id_level = $_COOKIE['id_level'];

                if($id_level==3)
                {
                    $default_page = 'dashboard';
                }
                
                else if($id_level==4)
                {
                    $default_page = 'dashboard_monitor';
                }
            
                if( isset($_GET['page']))
                {
                    $default_page = $_GET['page'];
                }

                include_once("view/".$default_page.".php");
            ?>

        </div>
        
    
                
    <?php require_once('components/script.php'); ?>
  </body>
</html>
