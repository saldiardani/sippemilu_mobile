<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:20px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="icon ion-arrow-left-a tx-teal" style="font-size: 30px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>&nbsp;&nbsp;&nbsp;Data DPT
    </label>
</div><!-- slim-pageheader -->

<div class="section-wrapper" style="width:100%">
    <p class="mg-b-20 mg-sm-b-40" style="margin-left: 10px;">Masukan Nomor Induk Kependudukan</p>
    <div class="row">
        <div class="col-lg" style="margin-left: 10px;margin-right: 10px;">
            <input class="form-control" id="nik" placeholder="4 digit angka terakhir tidak disertakan" type="text" required autocomplete="off">
            <button style="margin-top:20px" class="btn btn-outline-secondary active btn-block mg-b-10" id="search">Cari Data</button>
        </div><!-- col -->
    </div><!-- row -->
</div>

<script type="text/javascript">

    
    function backPage()
    {
        document.location.href="dptData";
    }

    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>