<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;Pemilih
    </label>
</div><!-- slim-pageheader -->


<?php
    $count  = (int) 0;

    $id_prov    = (int) 12;
    $id_kab     = (int) 1219;
    $id_kec     = $_COOKIE['id_kec'];
    $id_kel     = $_COOKIE['id_kel'];
    $tps        = $_COOKIE['tps'];

    $query      = "SELECT * FROM tbl_dpt WHERE tbl_dpt.id_province=? AND tbl_dpt.id_kota=? AND tbl_dpt.id_kec=? AND tbl_dpt.id_kel=? AND tbl_dpt.tps=?";
    $getAllData = $db->getAllValue($query,[$id_prov,$id_kab,$id_kec,$id_kel,$tps]);

?>

        <div class="card card-sales" style="width:100%;margin-top:10px;" id="detailtps<?php echo $count; ?>">
        <div class="table-responsive">
                <table class="table mg-b-0 tx-13">
                  <thead>
                    <tr class="tx-10">
                      <th class="wd-10p pd-y-5" style="vertical-align:middle;text-align:center">No</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">NO KK</th>
                      <th class="pd-y-5 tx-right" style="vertical-align:middle;text-align:center">NIK</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Nama Pemilih</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Tempat Lahir</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Tanggal Lahir</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Umur</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Status Kawin</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Jenis Kelamin</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Alamat</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">RT</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">RW</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Ket</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Disabilitas</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">TPS</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Telepon</th>
                    </tr>
                  </thead>
                  <tbody>

            <?php
                  foreach($getAllData as $data)
                  {
                      $count++;
                      $tps     = $data['tps'];
              ?>

                    <tr>
                      <td class="valign-middle"><?php echo $count; ?></td>
                      <td class="valign-middle"><?php echo $data['nokk'] ?></td>
                      <td class="valign-middle"><?php echo $data['nik'] ?></td>
                      <td class="valign-middle"><?php echo $data['nama_pemilih']?></td>
                      <td class="valign-middle"><?php echo $data['tempat_lahir'] ?></td>
                      <td class="valign-middle"><?php echo $data['tanggal_lahir'] ?></td>
                      <td class="valign-middle"><?php echo $data['umur'] ?></td>
                      <td class="valign-middle"><?php echo $data['status_kawin'] ?></td>
                      <td class="valign-middle"><?php echo $data['jk'] ?></td>
                      <td class="valign-middle"><?php echo $data['jalan'] ?></td>
                      <td class="valign-middle"><?php echo $data['rt'] ?></td>
                      <td class="valign-middle"><?php echo $data['rw'] ?></td>
                      <td class="valign-middle"><?php echo $data['ket'] ?></td>
                      <td class="valign-middle"><?php echo $data['disabilitas'] ?></td>
                      <td class="valign-middle"><?php echo $data['tps'] ?></td>
                      <td class="valign-middle"><?php echo $data['telepon'] ?></td>
                    </tr>
            <?php
                }
            ?>
                  </tbody>
                </table>
              </div>          
        </div>


<script type="text/javascript">

   
    function backPage()
    {
        document.location.href="formSearch";
    }


    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>