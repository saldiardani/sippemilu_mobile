<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:20px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="icon ion-arrow-left-a tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>&nbsp;&nbsp;&nbsp;suara
    </label>
</div><!-- slim-pageheader -->

<div class="section-wrapper" style="width:100%">
    <h6 class="tx-inverse mg-b-0">Menunggu input data DPT</h6>
</div>

<script type="text/javascript">

    function backPage()
    {
        document.location.href="dashboard_monitor";
    }

    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>