<div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0" style="width:50%" id="dpt">
    <div class="card card-status" >
        <div class="media" style="display: unset;">
        <i class="icon ion-ios-people-outline tx-purple"></i>
        <div class="media-body" style="margin-left:0">
            <p>Data DPT</p>
        </div><!-- media-body -->
        </div><!-- media -->
    </div><!-- card -->
    </div><!-- col-3 -->

    <div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0" style="width:50%" id="real_count">
    <div class="card card-status" >
        <div class="media" style="display: unset;">
        <i class="icon ion-ios-bookmarks-outline tx-teal"></i>
        <div class="media-body" style="margin-left:0">
            <p>Real Count</p>
        </div><!-- media-body -->
        </div><!-- media -->
    </div><!-- card -->
    </div><!-- col-3 -->

    <div class="col-sm-3 col-lg-2 mg-t-10 mg-lg-t-0" style="width:50%" id="about">
    <div class="card card-status">
        <div class="media" style="display: unset;">
        <i class="icon ion-pin tx-primary"></i>
        <div class="media-body" style="margin-left:0">
            <p>Tentang</p>
        </div><!-- media-body -->
        </div><!-- media -->
    </div><!-- card -->
    </div><!-- col-3 -->

    <div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0" style="width:50%" id="help">
    <div class="card card-status">
        <div class="media" style="display: unset;">
        <i class="icon ion-chatbubbles tx-pink"></i>
        <div class="media-body" style="margin-left:0">
            <p>Bantuan</p>
        </div><!-- media-body -->
        </div><!-- media -->
    </div><!-- card -->
</div><!-- col-3 -->

<script type="text/javascript">

    function realCount()
    {
        document.location.href="realCount";
    }

    function dptData()
    {
        document.location.href="dptData";
    }

    function about()
    {
        document.location.href="about";
    }

    function help()
    {
        document.location.href="help";
    }

    function readyApps()
    {
        $("#real_count").click(realCount);
        $("#dpt").click(dptData);
        $("#about").click(about);
        $("#help").click(help);
    }

    $(document).ready(readyApps);
</script>