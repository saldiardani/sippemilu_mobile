<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;Pemilih
    </label>
</div><!-- slim-pageheader -->

<!--
<div class="section-wrapper row no-gutters" style="width:100%;margin-top:10px" id="total_kec">
    <div class="col-lg-3">
        <div class="dash-content">
            <label class="tx-primary" style="margin-left: 10px;">Total Desa Terdaftar</label>
            <?php
                // $id_kec     = $_COOKIE['id_kec'];
                // $query      = "SELECT COUNT(DISTINCT id_kel) FROM tbl_dpt WHERE tbl_dpt.id_kec AND tbl_dpt.id_kel<>0";
                // $getAllData = $db->getValue($query,[$id_kec]);

                // foreach($getAllData as $data)
                // {
            ?>
                    <h2 style="margin-left: 10px;font-weight:300;"><?php echo $data." Kelurahan/Desa"?></h2>
            <?php      
                // }
            ?>
            
        </div>
    </div>
</div>
-->

<?php
    $count  = (int) 0;
    $id_kec = $_COOKIE['id_kec'];

    $query      = "SELECT DISTINCT id_kel FROM tbl_dpt WHERE tbl_dpt.id_kec=?";
    $getAllData = $db->getAllValue($query,[$id_kec]);
    
    foreach($getAllData as $data)
    {
        $count++;
        $id_kel     = $data['id_kel'];

        $query      = "SELECT * FROM tbl_kelurahan WHERE tbl_kelurahan.id_kel=?";
        $getData    = $db->getValue($query,[$id_kel]);
?>
        <div class="card card-sales" style="width:100%;margin-top:10px;" id="detailkel<?php echo $count; ?>">
            <input type="hidden" value="<?php echo $getData['id_kel'] ?>" id="idkel<?php echo $count ?>" />
            <h6 class="slim-card-title tx-primary" style="margin-left:5px"><?php echo $getData['nama_kel'] ?></h6>
            <!-- <div class="row"> -->
                <!-- <div class="col" style="margin-left: 5px;text-align: center;">
                    <label class="tx-12">Kel. / Desa</label>
                    <?php
                        $id_kec = $getData['id_kec'];
                        $query  = "SELECT COUNT(DISTINCT id_kel) FROM tbl_dpt WHERE id_kec=?;";
                        $getData_kel= $db->getValue($query,[$id_kec]);
                    ?>
                    <p><?php foreach($getData_kel as $data) { echo number_format($data); } ?></p>
                </div> -->
            
                <!-- <div class="col" style="text-align: center;">
                    <label class="tx-12">TPS</label>
                    <?php
                        $id_kec = $getData['id_kec'];
                        $query  = "SELECT COUNT(DISTINCT tps) FROM tbl_dpt WHERE id_kec=?;";
                        $getData_tps= $db->getValue($query,[$id_kec]);
                    ?>
                    <p><?php foreach($getData_tps as $data) { echo number_format($data); } ?></p>
                </div> -->
            
                <!-- <div class="col" style="text-align: center;">
                    <label class="tx-12">Pemlih Tetap</label>
                    <?php
                        $id_kec = $getData['id_kec'];
                        $query  = "SELECT COUNT(DISTINCT nama_pemilih) FROM tbl_dpt WHERE id_kec=?;";
                        $getData_pemilih= $db->getValue($query,[$id_kec]);
                    ?>
                    <p><?php foreach($getData_pemilih as $data) { echo number_format($data); } ?></p>
                </div> -->
            <!-- </div> -->

            <!-- <p class="tx-12 mg-b-0">Persentase Prediksi Pemilih Calon</p>
            <div class="progress mg-b-5">
                <div class="progress-bar bg-primary wd-50p" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>   
            </div> -->
            
        </div>
<?php
    }
?>



<script type="text/javascript">

    $(document).on('click','div[id^=detailkel]', goToKecDetail);

    function goToKecDetail()
    {
        var numbering_format    = parseInt ( this.id.replace ( 'detailkel' , '' ) , 10 );
        var id_kel              = $( '#idkel' + numbering_format ).val ();

        setCookies('id_kel', id_kel, 10);

        document.location.href='detailTPS ';
    }

    function backPage()
    {
        document.location.href="detailKec";
    }


    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>