<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:20px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="icon ion-arrow-left-a tx-teal" style="font-size: 30px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>&nbsp;&nbsp;&nbsp;Data DPT
    </label>
</div><!-- slim-pageheader -->

<div class="section-wrapper" style="width:100%">
    <p class="mg-b-20 mg-sm-b-40" style="margin-left: 10px;">Masukan Nomor Induk Kependudukan</p>
    <div class="row">
        <div class="col-lg" style="margin-left: 10px;margin-right: 10px;">
            <input class="form-control" id="nik" placeholder="4 digit angka terakhir tidak disertakan" type="text" required autocomplete="off">
            <button style="margin-top:20px" class="btn btn-outline-secondary active btn-block mg-b-10" id="search">Cari Data</button>
        </div><!-- col -->
    </div><!-- row -->
</div>

<script type="text/javascript">

    function getData()
    {
        var nik = $("#nik").val();
        
        if( nik=="")
        {
            alert('NIK harus diisi');
        }
        else
        {
            $.ajax
            ({
                type        : 'POST',
                url         : 'API/web/sippemilu.php',
                data        : 'type=reqgetdptdata'+'&nik='+nik,
                dataType    : 'JSON',
                cache       : false,
                success     : function(response)
                {
                    for(key in response)
                    {
                        if(response.hasOwnProperty(key))
                        {
                            if(response[key]['type']==='resgetdptdata')
                            {
                                if(response[key]['state'])
                                {
                                    var nokk = response[key]['nokk'];
                                    var nik = response[key]['nik'];
                                    var nama_pemilih = response[key]['nama_pemilih'];
                                    var tempat_lahir = response[key]['tempat_lahir'];
                                    var tanggal_lahir = response[key]['tanggal_lahir'];
                                    var umur = response[key]['umur'];
                                    var status_kawin = response[key]['status_kawin'];
                                    var jk = response[key]['jk'];
                                    var jalan = response[key]['jalan'];
                                    var rt = response[key]['rt'];
                                    var rw = response[key]['rw'];
                                    var ket = response[key]['ket'];

                                    setCookies("nokk",nokk,1);
                                    setCookies("nik",nik,1);
                                    setCookies("nama_pemilih",nama_pemilih,1);
                                    setCookies("tempat_lahir",tempat_lahir,1);
                                    setCookies("tanggal_lahir",tanggal_lahir,1);
                                    setCookies("umur",umur,1);
                                    setCookies("status_kawin",status_kawin,1);
                                    setCookies("jk",jk,1);
                                    setCookies("jalan",jalan,1);
                                    setCookies("rt",rt,1);
                                    setCookies("rw",rw,1);
                                    setCookies("ket",ket,1);

                                    document.location.href = "dptDataView";
                                }
                            }
                        }
                    }
                }
            });
            return false;
        }
    }

    function backPage()
    {
        document.location.href="dashboard";
    }

    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#search").click(getData);
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>