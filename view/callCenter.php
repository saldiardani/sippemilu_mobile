<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;Call Center
    </label>
</div><!-- slim-pageheader -->


<?php
    $count  = (int) 0;

    // $id_prov    = (int) 12;
    // $id_kab     = (int) 1219;
    // $id_kec     = $_COOKIE['id_kec'];
    // $id_kel     = $_COOKIE['id_kel'];
    // $tps        = $_COOKIE['tps'];

    // $query      = "SELECT * FROM tbl_dpt WHERE tbl_dpt.id_province=? AND tbl_dpt.id_kota=? AND tbl_dpt.id_kec=? AND tbl_dpt.id_kel=? AND tbl_dpt.tps=?";
    // $getAllData = $db->getAllValue($query,[$id_prov,$id_kab,$id_kec,$id_kel,$tps]);

    $id_level = (int) 3;
    
    $query = "SELECT * FROM tbl_user WHERE tbl_user.id_level=?";
    $getCallCenterData = $db->getAllValue($query,[$id_level]);
?>

        <div class="card card-sales" style="width:100%;margin-top:10px;" id="detailtps<?php echo $count; ?>">
        <div class="table-responsive">
                <table class="table mg-b-0 tx-13">
                  <thead>
                    <tr class="tx-10">
                      <th class="wd-10p pd-y-5" style="vertical-align:middle;text-align:center">No</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Nama</th>
                      <th class="pd-y-5" style="vertical-align:middle;text-align:center">Status</th>
                      <th class="pd-y-5 tx-right" style="vertical-align:middle;text-align:center">No Telepon</th>
                      <th class="pd-y-5 tx-right" style="vertical-align:middle;text-align:center">Kecamatan</th>
                      <th class="pd-y-5 tx-right" style="vertical-align:middle;text-align:center">Kelurahan</th>
                      <th class="pd-y-5 tx-right" style="vertical-align:middle;text-align:center">TPS</th>
                    </tr>
                  </thead>
                  <tbody>

            <?php
                foreach($getCallCenterData as $data)
                {
                    $count++;
              ?>

                    <tr>
                      <td class="valign-middle"><?php echo $count; ?></td>
                      <td class="valign-middle"><?php echo $data['fullname'] ?></td>
                      <td class="valign-middle"><?php echo 'relawan tps' ?></td>
                      <td class="valign-middle"><?php echo '081029308124' ?></td>
                      <td class="valign-middle"><?php echo 'Air Putih' ?></td>
                      <td class="valign-middle"><?php echo 'Aras' ?></td>
                      <td class="valign-middle"><?php echo '1' ?></td>
                    </tr>
            <?php
                }
            ?>
                  </tbody>
                </table>
              </div>          
        </div>


<script type="text/javascript">

   
    function backPage()
    {
        document.location.href="dashboard_monitor";
    }


    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>