<style>
       #map {
        height: 400px;
        width: 100%;
       }
    </style>

<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;GPS
    </label>
</div><!-- slim-pageheader -->



<div id="map"></div>


 <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxcmVbQisiTKf_6yMReOt6iiW3zoNhAIw&callback=initMap">
</script>

 <script>
      function initMap() {
        var uluru = {lat: 3.487398, lng: 98.7815003};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
</script>

<script type="text/javascript">
    function backPage()
    {
        document.location.href="dashboard_monitor";
    }


    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>