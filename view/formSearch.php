<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;Pemilih
    </label>
</div><!-- slim-pageheader -->


<div class="card card-sales" style="width:100%;margin-top:0px;" >
    <label class="form-control-label" style="margin-left: 5px;margin-top: 10px;font-weight:700">Kabupaten</label>
    <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih Kabupaten" tabindex="-1" aria-hidden="true" id="kotakab">
        <option value="" label="-- Pilih -- "></option>

        <?php
            $id_prov    = (int) 12;
            $query      = "SELECT * FROM tbl_kabupaten WHERE tbl_kabupaten.id_prov=? ORDER BY nama_kab ASC";

            $getKab = $db->getAllValue($query,[$id_prov]);

            print_r($getKab);

            foreach($getKab as $data)
            {
        ?>
                <option value="<?php echo $data['id_kab']?>"><?php echo $data['nama_kab'] ?></option>
        <?php
            }

        ?>
        
    </select>

    <label class="form-control-label" style="margin-left: 5px;margin-top: 20px;font-weight:700">Kecamatan</label>
    <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih Kecamatan" tabindex="-1" aria-hidden="true" id="kecamatan">
        <option value="" label="-- Pilih -- "></option>
        
    </select>

    <label class="form-control-label" style="margin-left: 5px;margin-top: 20px;font-weight:700">Kelurahan</label>
    <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih Kelurahan" tabindex="-1" aria-hidden="true" id="kelurahan">
        <option value="" label="-- Pilih -- "></option>
        
    </select>

    <label class="form-control-label" style="margin-left: 5px;margin-top: 20px;font-weight:700">TPS</label>
    <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih TPS" tabindex="-1" aria-hidden="true" id="tps">
        <option value="" label="-- Pilih -- "></option>
        
    </select>

    <button class="btn btn-primary bd-0" style="margin-top: 20px;font-weight:300" id="viewData">Lihat Data</button>
</div>

<script type="text/javascript">

    function clearThree()
    {
        $('#kotakab').html("");
        $('#kecamatan').html("");
        $('#kelurahan').html("");

        var option = '<option value=\"\">-- Pilih --</option>';

        $('#kotakab').append(option);
        $('#kecamatan').append(option);
        $('#kelurahan').append(option);
    }


    function clearTwo()
    {
        $('#kecamatan').html("");
        $('#kelurahan').html("");

        var option = '<option value=\"\">-- Pilih --</option>';

        $('#kecamatan').append(option);
        $('#kelurahan').append(option);
    }

    function clearOne()
    {
        $('#kelurahan').html("");

        var option = '<option value=\"\">-- Pilih --</option>';

        $('#kelurahan').append(option);
    }





    function getKec()
    {
        clearTwo();

        var id_kokab = $("#kotakab").val();
        var option      = null;
        var firstime    = true;
        
        $.ajax
        ({
            type        : 'POST',
            url         : 'API/web/sippemilu.php',
            data        : 'type=reqgetkec'+'&id_kokab='+id_kokab,
            dataType    : 'JSON',
            cache       : false,
            success     : function(response)
            {
                $('#kecamatan').html("");

                for(key in response)
                {
                    if(response.hasOwnProperty(key))
                    {
                        if(response[key]['type']==='resgetkec')
                        {
                            if(response[key]['state'])
                            {
                                var id_kec = response[key]["id_kec"];

                                if(firstime)
                                {
                                    option = '<option value=\"\">Pilih Data Tersedia</option>';
                                    $('#kecamatan').append(option);

                                    option = '<option value=\"'+id_kec+'\">'+response[key]['name']+'</option>';
                                    $('#kecamatan').append(option);

                                    firstime = false;
                                }
                                else
                                {
                                    option = '<option value=\"'+id_kec+'\">'+response[key]['name']+'</option>';
                                    $('#kecamatan').append(option);
                                }

                                
                            }
                        }
                    }
                }
            }
        });
    }


    function getKel()
    {
        clearOne();

        var id_kec = $("#kecamatan").val();
        var option      = null;
        var firstime    = true;
        
        $.ajax
        ({
            type        : 'POST',
            url         : 'API/web/sippemilu.php',
            data        : 'type=reqgetkel'+'&id_kec='+id_kec,
            dataType    : 'JSON',
            cache       : false,
            success     : function(response)
            {
                $('#kelurahan').html("");

                for(key in response)
                {
                    if(response.hasOwnProperty(key))
                    {
                        if(response[key]['type']==='resgetkel')
                        {
                            if(response[key]['state'])
                            {
                                var id_kec = response[key]["id_kel"];

                                if(firstime)
                                {
                                    option = '<option value=\"\">Pilih Data Tersedia</option>';
                                    $('#kelurahan').append(option);

                                    option = '<option value=\"'+id_kec+'\">'+response[key]['name']+'</option>';
                                    $('#kelurahan').append(option);

                                    firstime = false;
                                }
                                else
                                {
                                    option = '<option value=\"'+id_kec+'\">'+response[key]['name']+'</option>';
                                    $('#kelurahan').append(option);
                                } 
                            }
                            else
                            {
                                option = '<option value=\"\">Data Tidak Tersedia</option>';
                                $('#kelurahan').append(option);
                            }
                        }
                    }
                }
            }
        });
    }

    function getTPS()
    {
        var id_kel = $("#kelurahan").val();
        var option      = null;
        var firstime    = true;

        $.ajax
        ({
            type        : 'POST',
            url         : 'API/web/sippemilu.php',
            data        : 'type=reqgettps'+'&id_kel='+id_kel,
            dataType    : 'JSON',
            cache       : false,
            success     : function(response)
            {
                $('#tps').html("");

                for(key in response)
                {
                    if(response.hasOwnProperty(key))
                    {
                        if(response[key]['type']==='resgettps')
                        {
                            if(response[key]['state'])
                            {
                                var tps = response[key]['tps'];

                                if(firstime)
                                {
                                    option = '<option value=\"\">Pilih Data Tersedia</option>';
                                    $('#tps').append(option);

                                    option = '<option value=\"'+tps+'\">'+response[key]['tps']+'</option>';
                                    $('#tps').append(option);

                                    firstime = false;
                                }
                                else
                                {
                                    option = '<option value=\"'+tps+'\">'+response[key]['tps']+'</option>';
                                    $('#tps').append(option);
                                } 
                            }
                            else
                            {
                                option = '<option value=\"\">Data Tidak Tersedia</option>';
                                $('#tps').append(option);
                            }
                        }
                    }
                }
            }
        });
    }

    function storeIDRegional()
    {
        var id_kab  = $("#kotakab").val();
        var id_kec  = $("#kecamatan").val();
        var id_kel  = $("#kelurahan").val();
        var tps     = $("#tps").val();

        id_kab  = (id_kab=="") ? 0 : id_kab;
        id_kec  = (id_kec=="") ? 0 : id_kec;
        id_kel  = (id_kel=="") ? 0 : id_kel;
        tps     = (tps=="") ? 0 : tps;

        setCookies("id_kab", id_kab ,1);
        setCookies("id_kec", id_kec ,1);
        setCookies("id_kel", id_kel ,1);
        setCookies("tps", tps ,1);

        document.location.href='detailPemilih';
    }

    function backPage()
    {
        document.location.href="dashboard_monitor";
    }

    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
        $("#kotakab").change(getKec);
        $("#kecamatan").change(getKel);
        $("#kelurahan").change(getTPS);

        $("#viewData").click(storeIDRegional);
    }

    $(document).ready(readyApps);
</script>