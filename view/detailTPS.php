<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;Pemilih
    </label>
</div><!-- slim-pageheader -->


<?php
    $count  = (int) 0;
    $id_kel = $_COOKIE['id_kel'];

    $query      = "SELECT DISTINCT tps FROM tbl_dpt WHERE tbl_dpt.id_kel=?";
    $getAllData = $db->getAllValue($query,[$id_kel]);
    
    foreach($getAllData as $data)
    {
        $count++;
        $tps     = $data['tps'];
?>
        <div class="card card-sales" style="width:100%;margin-top:10px;" id="detailtps<?php echo $count; ?>">
            <input type="hidden" value="<?php echo $tps ?>" id="tps<?php echo $count ?>" />
            <h6 class="slim-card-title tx-primary" style="margin-left:5px"><?php echo "Tps - ".$tps ?></h6> 
        </div>
<?php
    }
?>



<script type="text/javascript">

    $(document).on('click','div[id^=detailtps]', goToKecDetail);

    function goToKecDetail()
    {
        var numbering_format = parseInt ( this.id.replace ( 'detailtps' , '' ) , 10 );
        var tps              = $( '#tps' + numbering_format ).val ();

        setCookies('tps', tps, 10);

        document.location.href='detailPemilih ';
    }

    function backPage()
    {
        document.location.href="detailKel";
    }


    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>