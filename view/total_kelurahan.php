<?php 
    ini_set('memory_limit', '-1');
?>
<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;Detail Kelurahan / Desa
    </label>
</div><!-- slim-pageheader -->

<div class="section-wrapper row no-gutters" style="width:100%;margin-top:10px" id="total_kel">
    <div class="col-lg-3">
        <div class="dash-content">
            <label class="tx-primary" style="margin-left: 10px;">Total Desa Terdaftar</label>
            <?php
                $query      = "SELECT COUNT(DISTINCT id_kel) FROM tbl_dpt;";
                $getAllData = $db->getValue($query);

                foreach($getAllData as $data)
                {
            ?>
                    <h2 style="margin-left: 10px;font-weight:300;"><?php echo $data." Kelurahan/Desa"?></h2>
            <?php      
                }
            ?>
            
        </div><!-- dash-content -->
    </div>
</div>

<?php 
    $query      = "SELECT DISTINCT id_kec FROM tbl_dpt WHERE tbl_dpt.id_kec<>0";
    $getAllData = $db->getAllValue($query);
    
    foreach($getAllData as $data)
    {
        $id_kec = $data['id_kec'];

        if($id_kec==0)
        {

        }
        else
        {

            $query  = "SELECT * FROM tbl_kecamatan WHERE tbl_kecamatan.id_kec=?";
            $getKec = $db->getValue($query,[$id_kec]);
?>
            <div class="card card-sales" style="width:100%;margin-top:10px;">
            <h6 class="slim-card-title tx-primary" style="margin-left:5px"><?php echo $getKec['nama_kec'] ?></h6>
            
            <?php
                $query  = "SELECT DISTINCT id_kel FROM tbl_dpt WHERE tbl_dpt.id_kec=?";
                $getKel = $db->getAllValue($query,[$id_kec]);

                foreach($getKel as $dataKel)
                {
                    $id_kel = $dataKel['id_kel'];

                    $query  = "SELECT * FROM tbl_kelurahan WHERE tbl_kelurahan.id_kel=?";
                    $getKel = $db->getValue($query,[$id_kel]);

                    $query      = "SELECT COUNT(DISTINCT tps) FROM tbl_dpt WHERE tbl_dpt.id_kel=?";
                    $countTPS   = $db->getValue($query,[$id_kel]);

                    $query          = "SELECT COUNT(DISTINCT nama_pemilih) FROM tbl_dpt WHERE tbl_dpt.id_kel=?";
                    $countPemilih   = $db->getValue($query,[$id_kel]);
            ?>
                    <div class="row">
                        <div class="col" style="margin-left: 5px;text-align: center;">
                            <label class="tx-12">Kel. / Desa</label>
                            <p style="font-size: 12px;"><?php echo $getKel['nama_kel']; ?></p>
                        </div><!-- col -->

                        <div class="col" style="text-align: center;">
                            <label class="tx-12">TPS</label>

                            <p style="font-size: 12px;"><?php foreach($countTPS as $dataTPS ){ echo $dataTPS; }?></p>
                        </div><!-- col -->
                    
                        <div class="col" style="text-align: center;">
                            <label class="tx-12">Pemlih Tetap</label>
                            <p style="font-size: 12px;"><?php foreach($countPemilih as $dataPemilih ){ echo number_format($dataPemilih); }?></p>
                        </div><!-- col -->
                    </div><!-- row -->

                    <hr />
            <?php
                }
            ?>

            
        </div>
<?php
        }
    }  
?>



<script type="text/javascript">

    function backPage()
    {
        document.location.href="pemilih";
    }


    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>