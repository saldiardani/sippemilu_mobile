<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;Real Count
    </label>
</div><!-- slim-pageheader -->


<div class="card card-dash-one mg-t-20">
          <div class="row no-gutters">
            <div class="col-lg-3">
              <i class="fas fa-user-circle" style="font-size: 70px;color:#694167"></i>
              <div class="dash-content">
                <label class="tx-primary">Harry Nugroho && Muhammad Syafi'i</label>
                <h3>0</h3>
              </div><!-- dash-content -->
            </div><!-- col-3 -->

            <div class="col-lg-3">
              <i class="fas fa-user-circle" style="font-size: 70px;color:#494169"></i>
              <div class="dash-content">
                <label class="tx-success">Darwis && Janmat Sembiring</label>
                <h3>0</h3>
              </div><!-- dash-content -->
            </div><!-- col-3 -->

            <div class="col-lg-3">
              <i class="fas fa-user-circle" style="font-size: 70px;color:#416967;"></i>
              <div class="dash-content">
                <label class="tx-purple">Zahir && Oku Iqbal Prirma</label>
                <h3>0</h3>
              </div><!-- dash-content -->
            </div><!-- col-3 -->

            <div class="col-lg-3">
              <i class="fas fa-user-circle" style="font-size: 70px;color: #988686;"></i>
              <div class="dash-content">
                <label class="tx-danger">Khariril Anwar && Sofyan Alwi</label>
                <h3>0</h3>
              </div><!-- dash-content -->
            </div><!-- col-3 -->

          </div><!-- row -->
        </div>


<script type="text/javascript">

   
    function backPage()
    {
        document.location.href="dashboard_monitor";
    }


    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
    }

    $(document).ready(readyApps);
</script>