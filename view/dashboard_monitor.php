<div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0" style="width: 100%;">
  <?php ?>
  <h4 class="tx-inverse mg-b-0 tx-light">Selamat sore, <?php echo $_COOKIE['username'] ?></h4>
  <h5 class="tx-inverse mg-b-0 tx-light" style="font-size: 12px;">Semangat dalam beraktivitas</h5>
</div>

<div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0" style="width: 50%;margin-top: 60px;" id="pemilih">
    <div class="card card-status" >
      <div class="media" style="text-align: center;display: unset;">
        <i class="fas fa-child" style="font-size: 44px;color: #8297ab;"></i>
        <div class="media-body" style="margin-top: 10px;font-size: 11px;">
          <p>Relawan dan Pemilih</p>
        </div><!-- media-body -->
      </div><!-- media -->
    </div><!-- card -->
</div><!-- col-3 -->

<div class="col-sm-6 col-lg-3 mg-t-10 mg-sm-t-0" style="width: 50%;margin-top: 60px;" id="callcenter">
  <div class="card card-status" >
    <div class="media" style="text-align: center;display: unset;">
      <i class="fas fa-phone" style="font-size: 40px;color: #8297ab;"></i>
      <div class="media-body" style="margin-top: 20px;font-size: 11px;">
        <p>Call Center</p>
      </div><!-- media-body -->
    </div><!-- media -->
  </div><!-- card -->
</div><!-- col-3 -->

<div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0" style="width: 50%;" id="smscenter">
  <div class="card card-status" >
    <div class="media" style="text-align: center;display: unset;">
      <i class="fas fa-envelope-open" style="font-size: 40px;color: #8297ab;"></i>
      <div class="media-body" style="margin-top: 20px;font-size: 11px;">
        <p>SMS Center</p>
      </div><!-- media-body -->
    </div><!-- media -->
  </div><!-- card -->
</div><!-- col-3 -->

<div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0" style="width: 50%;" id="stats">
  <div class="card card-status" >
    <div class="media" style="text-align: center;display: unset;">
      <i class="fas fa-bullhorn" style="font-size: 40px;color: #8297ab;"></i>
      <div class="media-body" style="margin-top: 20px;font-size: 11px;">
        <p>Graphic Statistic</p>
      </div><!-- media-body -->
    </div><!-- media -->
  </div><!-- card -->
</div><!-- col-3 -->

<div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0" style="width: 50%;" id="polling">
  <div class="card card-status" >
    <div class="media" style="text-align: center;display: unset;">
    <i class="fas fa-check-square" style="font-size: 40px;color: #8297ab;"></i>
      <div class="media-body" style="margin-top: 20px;font-size: 11px;">
        <p>Survey dan Polling</p>
      </div><!-- media-body -->
    </div><!-- media -->
  </div><!-- card -->
</div><!-- col-3 -->

<div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0" style="width: 50%;" id="realcount">
  <div class="card card-status" >
    <div class="media" style="text-align: center;display: unset;">
      <i class="fas fa-sort-numeric-up" style="font-size: 40px;color: #8297ab;"></i>
      <div class="media-body" style="margin-top: 20px;font-size: 11px;">
        <p>Real Count</p>
      </div><!-- media-body -->
    </div><!-- media -->
  </div><!-- card -->
</div><!-- col-3 -->

<div class="col-sm-6 col-lg-3 mg-t-10 mg-lg-t-0" style="width: 50%;" id="gps">
  <div class="card card-status" >
    <div class="media" style="text-align: center;display: unset;">
    <i class="fab fa-safari" style="font-size: 40px;color: #8297ab;"></i>
      <div class="media-body" style="margin-top: 20px;font-size: 11px;">
        <p>GPS Inputer</p>
      </div><!-- media-body -->
    </div><!-- media -->
  </div><!-- card -->
</div><!-- col-3 -->

<script type="text/javascript">

  function pemilihPage()
  {
    document.location.href='formSearch'
  }

  function smsCenterPage()
  {
    document.location.href='smsCenter';
  }

  function callCenterPage()
  {
    document.location.href='callCenter';
  }

  function realCountPage()
  {
    document.location.href='realCount';
  }

  function statsPage()
  {
    document.location.href='statistic';
  }

  function gpsPage()
  {
    document.location.href='gps';
  }

  function readyApps()
  {
    $("#pemilih").click(pemilihPage);
    $("#smscenter").click(smsCenterPage);
    $("#callcenter").click(callCenterPage);
    $("#realcount").click(realCountPage);
    $("#stats").click(statsPage);
    $("#gps").click(gpsPage);
    // $("#relawan").click(relawanPage);
    // $("#inputer").click(inputerPage);
    // $("#suara").click(suaraPage);
  }

  $(document).ready(readyApps);
</script>