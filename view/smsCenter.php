<div class="slim-pageheader">
    <label class="section-title" style="margin-left:10px;margin-top:0px;font-size:15px;font-weight: 400;letter-spacing: 1px;">
        <i id="back" class="fas fa-arrow-left tx-teal" style="font-size: 15px;padding: 8px;border-radius: 0px;background-color: #288be8;color: #fff;"></i>
        &nbsp;&nbsp;&nbsp;SMS Center
    </label>
</div><!-- slim-pageheader -->


<div class="card card-sales" style="width:100%;margin-top:0px;" >
    <label class="form-control-label" style="margin-left: 5px;margin-top: 10px;font-weight:700">Tujuan Pengriminan</label>
    <select class="form-control select2 select2-hidden-accessible" data-placeholder="Pilih Kabupaten" tabindex="-1" aria-hidden="true" id="tujuan">
        <option value="" label="-- Pilih -- "></option>
        <option value="0">Relawan</option>
        <option value="1">Pemilih</option>
    </select>

    <label class="form-control-label" style="margin-left: 5px;margin-top: 20px;font-weight:700">Pesan</label>
    <textarea rows="10" class="form-control" placeholder="Tulis Pesan" id="message"></textarea>


    <button class="btn btn-primary bd-0" style="margin-top: 20px;font-weight:300" id="sendMessage">Kirim Data</button>
</div>

<script type="text/javascript">

    function backPage()
    {
        document.location.href="dashboard_monitor";
    }

    function sendMessage()
    {
        alert('pesan akan dikirim ke tujuan. Terima kasih');
        $("#message").html("");
        $("#tujuan").val("");
    }

    function setCookies(cname, cvalue, exdays)
    {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function readyApps()
    {
        $("#back").click(backPage);
        $("#sendMessage").click(sendMessage);
    }

    $(document).ready(readyApps);
</script>